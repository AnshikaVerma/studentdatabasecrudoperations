import  java.util.*;
import java.io.*;

class student{
    static String studentName;
    static int studentRollNo;
    static String studentDateOfBirth;
    Scanner sc = new Scanner(System.in);

    public student() {
        System.out.println("Enter student name: ");
        studentName = sc.next();
        System.out.println("Enter student roll no: ");
        studentRollNo = sc.nextInt();
        System.out.println("Enter student D.O.B: ");
        studentDateOfBirth = sc.next();
    }

    public static void addStudentDetails(HashMap studentDetails){
        studentDetails.put(studentRollNo,new ArrayList<>(Arrays.asList(studentName,studentDateOfBirth)));
    }
    public static void getStudentDetails(HashMap studentDetails)
    {
            System.out.println(studentDetails);

    }
    public static void deleteStudentDetails(HashMap studentDetails, int rollNo){
        studentDetails.remove(rollNo);
    }
    public static void updateStudentDetails(HashMap studentDetails, String studentName, int studentRollNo, String studentDateOfBirth){
        studentDetails.put(studentRollNo,new ArrayList<>(Arrays.asList(studentName,studentDateOfBirth)));
    }
}



public class StudentData {
    public static void main(String[] args) {
        HashMap<Integer,ArrayList<String>> studentDetails=new HashMap<>();
        student.updateStudentDetails(studentDetails,"Manju",1,"10-02-2001");
        student.updateStudentDetails(studentDetails,"Jaya",2,"12-05-2000");
        student.updateStudentDetails(studentDetails,"Meena",3,"24-09-1991");
        student.updateStudentDetails(studentDetails,"Rekha",4,"05-01-1998");
        student.updateStudentDetails(studentDetails,"Siya",5,"31-12-2002");

        Scanner sc = new Scanner(System.in);
        boolean adminAccess = true;
        String password;
        System.out.println("Enter password in order to modify database.");
        password = sc.next();
        if(password.equals("12345")){
                System.out.println("Choose what operation you want to perform on database.");
                System.out.println("1.For adding new student details.");
                System.out.println("2.For deleting existing student detail.");
                System.out.println("3.For updating existing student details.");
                System.out.println("4.View student details.");
                int option = sc.nextInt();
                while(adminAccess) {
                    switch (option) {
                        case 1:
                            new student();
                            student.addStudentDetails(studentDetails);
                            System.out.println("Data added successfully.");
                            break;
                        case 2:
                            System.out.println("Enter the student roll no whose data you want to delete.");
                            int rollNo = sc.nextInt();
                            student.deleteStudentDetails(studentDetails, rollNo);
                            System.out.println("Data deleted successfully.");
                            break;
                        case 3:
                            System.out.println("Enter student roll no whose data you want to update : ");
                            int studentRollNo = sc.nextInt();
                            System.out.println("Enter new student name: ");
                            String studentName = sc.next();
                            System.out.println("Enter new student date of birth: ");
                            String studentDOB = sc.next();
                            student.updateStudentDetails(studentDetails,studentName,studentRollNo,studentDOB);
                            System.out.println("Data updated successfully.");
                            break;
                        case 4:
                            student.getStudentDetails(studentDetails);
                            break;
                        default:
                            System.out.println("Please, choose a valid option.");
                    }
                    System.out.println("Do you want to continue: true/false");
                    adminAccess = sc.nextBoolean();
                    if(adminAccess){
                        System.out.println("Choose what operation you want to perform on database.");
                        System.out.println("1.For adding new student details.");
                        System.out.println("2.For deleting existing student detail.");
                        System.out.println("3.For updating existing student details.");
                        System.out.println("4.View student details.");
                        option = sc.nextInt();
                    }
                }
        }
        else{
            boolean visibility;
            System.out.println("You don't have access to modify database but you can view it.");
            System.out.println("Do you want to view database: true/false");
            visibility = sc.nextBoolean();
            if(visibility) {
                student.getStudentDetails(studentDetails);
            }
        }
    }
}
